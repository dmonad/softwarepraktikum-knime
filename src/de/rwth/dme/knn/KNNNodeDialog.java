package de.rwth.dme.knn;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;

import javax.swing.JOptionPane;

import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.StringValue;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NotConfigurableException;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentButton;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.DialogComponentString;
import org.knime.core.node.defaultnodesettings.DialogComponentStringSelection;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

/**
 * <code>NodeDialog</code> for the "KNN" Node.
 * 
 * @author Manuel Daniel
 */
public class KNNNodeDialog extends DefaultNodeSettingsPane
{

    private KNNNodeSettings settings = new KNNNodeSettings();
    private DialogComponentStringSelection dfComponent;

    /**
     * New pane for configuring the KNN node.
     */
    protected KNNNodeDialog()
    {
        
        // -------------- Settings -----------------------
        createNewGroup("Settings:");
        
        addDialogComponent(new DialogComponentNumber(settings.kModel, "k:", 1));
        addDialogComponent(new DialogComponentBoolean(
                settings.addClassProbabilitiesColumnsModel,
                "Add class probabilities columns"));
        addDialogComponent(new DialogComponentBoolean(
                settings.addProbabilityColumnModel,
                "Add probability column"));
        addDialogComponent(new DialogComponentBoolean(
                settings.weightingByDistanceModel,
                "Weight neighbours by distance"));

        addDialogComponent(new DialogComponentColumnNameSelection(
                settings.classColumnModel, "Training data class column:", 0,
                true, StringValue.class));

		DialogComponentString classDup = new DialogComponentString(
				settings.outputClassColumnModel, "Columnname for assigned class", true, 10);
		addDialogComponent(classDup);

        createNewGroup("Distance functions:"); 

		DialogComponentButton addButton = new DialogComponentButton("Add");
        addButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                addDistanceFunctionClick();
            }
        });
        addDialogComponent(addButton);

        DialogComponentButton removeButton = new DialogComponentButton("Remove");
        removeButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                removeDistanceFunctionClick();
            }
        });
        addDialogComponent(removeButton);

        dfComponent = new DialogComponentStringSelection(
                settings.distanceFunctionModel, "Distance function",
                KNNNodePlugin.getDistanceFunctions());

        addDialogComponent(dfComponent);
        addDialogComponent(new DialogComponentNumber(settings.pForMinkowski,
                "p (for Minkowski)", 1));

    }

    private void addDistanceFunctionClick()
    {
        String clsName = (String) JOptionPane
                .showInputDialog(
                        null,
                        "Please enter the full name of the distance function class\n"+
                        "with an optional search path like "+
                        "'C:\\distance functions\\, my.package.MyFunction'",
                        "Add distance funtion", JOptionPane.PLAIN_MESSAGE,
                        null, null,
                        "de.rwth.dme.knn.distance.MinkowskiDistanceFunction");

        // User canceled
        if (clsName == null || clsName.equals(""))
        {
            return;
        }

        Preferences preferences = ConfigurationScope.INSTANCE.getNode(
                "de.rwth.dme.knn.KNNNodeFactory").node("distanceFunctions");

        // Try to load the specified distance function and show a message on errors
        IDistanceFunction df;
        try
        {
            df = KNNNodePlugin.getDefault().loadDistanceFunction(clsName);
        }
        catch (ClassNotFoundException e)
        {
            JOptionPane.showMessageDialog(null,
                    "The distance function was not found", "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        catch (InstantiationException e)
        {
            JOptionPane.showMessageDialog(null,
                    "The distance function could not be instanciated", "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        catch (IllegalAccessException e)
        {
            JOptionPane.showMessageDialog(null,
                    "The distance function could not be accessed", "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        catch (MalformedURLException e)
        {
            JOptionPane.showMessageDialog(null,
                    "The specified location could not be found", "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        // Save the added distance function
        try
        {
            preferences.put(df.getName(), clsName);
            preferences.flush();
        }
        catch (BackingStoreException e)
        {
            JOptionPane.showMessageDialog(null,
                    "Could not update distance funtion list", "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        // Add it to the runtime configuration and refresh the select box
        KNNNodePlugin.addDistanceFunction(df);
        dfComponent.replaceListItems(KNNNodePlugin.getDistanceFunctions(), null);
    }

    private void removeDistanceFunctionClick()
    {
        // Cannot remove the last distance function
        if (KNNNodePlugin.getDistanceFunctions().size() < 2)
        {
            JOptionPane.showMessageDialog(null,
                    "Cannot remove the last distance function", "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        IDistanceFunction df = settings.getDistanceFunction();

        // Show confirmation dialog
        if (JOptionPane.showConfirmDialog(
                null,
                "Do you really want to remove the distance function '"
                        + df.getName() + "'?", "Remove distance funtion",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
        {
            Preferences preferences = ConfigurationScope.INSTANCE.getNode(
                    "de.rwth.dme.knn.KNNNodeFactory").node("distanceFunctions");

            String className = df.getClass().getName();

            // Remove the selected distance function from the plugin preferences
            try
            {
                for (String key : preferences.keys())
                {
                    if (preferences.get(key, null).equals(className))
                    {
                        preferences.remove(key);
                    }
                }

                preferences.flush();
            }
            catch (BackingStoreException e)
            {
                JOptionPane.showMessageDialog(null,
                        "Could not update distance funtion list", "Error",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }

            // Remove the selected distance function from the runtime configuration and refresh the select box
            KNNNodePlugin.removeDistanceFunction(df);
            dfComponent.replaceListItems(KNNNodePlugin.getDistanceFunctions(),
                    null);
        }
    }

    @Override
    protected void loadSettingsFrom(final NodeSettingsRO settings,
            final DataTableSpec[] specs) throws NotConfigurableException
    {
        try
        {
            this.settings.loadSettings(settings);
        }
        catch (InvalidSettingsException ex)
        {
            // Ignore errors and use the defaults
        }
    }
}
