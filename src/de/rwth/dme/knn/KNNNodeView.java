package de.rwth.dme.knn;

import org.knime.core.node.NodeView;

/**
 * <code>NodeView</code> for the "KNN" Node.
 * 
 *
 * @author Daniel, Manuel
 */
public class KNNNodeView extends NodeView<KNNNodeModel> {

    /**
     * Creates a new view.
     * 
     * @param nodeModel The model (class: {@link KNNNodeModel})
     */
    protected KNNNodeView(final KNNNodeModel nodeModel) {
        super(nodeModel);
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void modelChanged() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onClose() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onOpen() {
        // TODO: generated method stub
    }

}

