package de.rwth.dme.knn;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;
import org.knime.core.node.defaultnodesettings.SettingsModelString;


//@author Daniel, Manuel

public class KNNNodeSettings
{
    public final SettingsModelInteger kModel = new SettingsModelInteger("k", 1);
    public final SettingsModelBoolean weightingByDistanceModel = new SettingsModelBoolean(
            "weightingByDistance", false);
    public final SettingsModelBoolean addClassProbabilitiesColumnsModel = new SettingsModelBoolean(
            "addClassProbabilitiesColumns", false);
    public final SettingsModelBoolean addProbabilityColumnModel = new SettingsModelBoolean(
            "addProbabilityColumnModel", false);
    public final SettingsModelString distanceFunctionModel = new SettingsModelString(
            "distanceFunction", null);
    public final SettingsModelIntegerBounded pForMinkowski = new SettingsModelIntegerBounded(
            "pForMinkowski", 2, 1, Integer.MAX_VALUE);
    public final SettingsModelString classColumnModel = new SettingsModelString(
            "classColumn", null);
    public final SettingsModelString outputClassColumnModel = new SettingsModelString(
    		"classDupColumnModel", "class");

    private IDistanceFunction distanceFunction;

    public KNNNodeSettings()
    {
        distanceFunctionModel.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent arg0)
            {
                distanceFunction = KNNNodePlugin
                        .getDistanceFunction(distanceFunctionModel
                                .getStringValue());
            }
        });
    }

    public int getK()
    {
        return kModel.getIntValue();
    }

    public void setK(int k)
    {
        kModel.setIntValue(k);
    }

    public boolean isAddClassProbabilitiesColumns()
    {
        return addClassProbabilitiesColumnsModel.getBooleanValue();
    }

    public boolean isAddProbabilityColumn()
    {
        return addProbabilityColumnModel.getBooleanValue();
    }

    public void setAddClassProbabilitiesColumns(
            boolean addClassProbabilitiesColumns)
    {
        addClassProbabilitiesColumnsModel
                .setBooleanValue(addClassProbabilitiesColumns);
    }

    public boolean isWeightingByDistance()
    {
        return weightingByDistanceModel.getBooleanValue();
    }

    public void setWeightingByDistance(boolean weightingByDistance)
    {
        weightingByDistanceModel.setBooleanValue(weightingByDistance);
    }

    public IDistanceFunction getDistanceFunction()
    {
        return distanceFunction;
    }

    public void setDistanceFunction(IDistanceFunction distanceFunction)
    {
        this.distanceFunction = distanceFunction;

        distanceFunctionModel.setStringValue(distanceFunction.getName());
    }

    public void setDistanceFunction(String distanceFunction)
    {
        this.distanceFunction = KNNNodePlugin
                .getDistanceFunction(distanceFunction);

        distanceFunctionModel.setStringValue(distanceFunction);
    }

    public void saveSettings(final NodeSettingsWO settings)
    {
        kModel.saveSettingsTo(settings);
        distanceFunctionModel.saveSettingsTo(settings);
        weightingByDistanceModel.saveSettingsTo(settings);
        addClassProbabilitiesColumnsModel.saveSettingsTo(settings);
        addProbabilityColumnModel.saveSettingsTo(settings);
        classColumnModel.saveSettingsTo(settings);
        pForMinkowski.saveSettingsTo(settings);
        outputClassColumnModel.saveSettingsTo(settings);
    }

    public void loadSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
        kModel.loadSettingsFrom(settings);
        distanceFunctionModel.loadSettingsFrom(settings);
        weightingByDistanceModel.loadSettingsFrom(settings);
        addClassProbabilitiesColumnsModel.loadSettingsFrom(settings);
        addProbabilityColumnModel.loadSettingsFrom(settings);
        classColumnModel.loadSettingsFrom(settings);
        pForMinkowski.loadSettingsFrom(settings);
        outputClassColumnModel.loadSettingsFrom(settings);
    }

    public void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
        kModel.validateSettings(settings);
        distanceFunctionModel.validateSettings(settings);
        weightingByDistanceModel.validateSettings(settings);
        addClassProbabilitiesColumnsModel.validateSettings(settings);
        addProbabilityColumnModel.validateSettings(settings);
        classColumnModel.validateSettings(settings);
        pForMinkowski.validateSettings(settings);
        outputClassColumnModel.validateSettings(settings);
    }

    public String getClassColumn()
    {
        return classColumnModel.getStringValue();
    }

    public void setClassColumn(String classColumn)
    {
        classColumnModel.setStringValue(classColumn);
    }    
    
    public String getDupClassColumn()
    {
        return classColumnModel.getStringValue();
    }

    public void setDupClassColumn(String classColumn)
    {
        classColumnModel.setStringValue(classColumn);
    }

}
