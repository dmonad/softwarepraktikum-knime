package de.rwth.dme.knn;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.RowKey;
import org.knime.core.data.StringValue;
import org.knime.core.data.container.CellFactory;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.util.MutableDouble;

class TrainingData
{
    public String Class;

    public DataCell[] Data;

    public double Distance;
}

/**
 * This is the model implementation of KNN.
 * 
 * @author Daniel, Manuel
 */
public class KNNNodeModel extends NodeModel
{
    /**
     * The node's settings
     */
    KNNNodeSettings settings = new KNNNodeSettings();

    /**
     * Internally used collection of the training data
     */
    private TrainingData[] trainingData;

    /**
     * A mapping from the instance data's column indices to the training data's
     * column indices.
     */
    private final List<Integer> columnMappig = new ArrayList<Integer>();

    /**
     * Constructor for the node model.
     */
    protected KNNNodeModel()
    {
        super(2, 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception
    {
        // Build the column mapping from instance -> training
        createColumnMapping(inData[0].getDataTableSpec(),
                inData[1].getDataTableSpec());
        createTrainingData(inData[0]);

        // If the desired class column name is already present -> error
        if (inData[1].getDataTableSpec().containsName(
                settings.outputClassColumnModel.getStringValue()))
        {
            throw new InvalidSettingsException(
                    "The instance data does already contain a column with the name '"
                            + settings.outputClassColumnModel.getStringValue()
                            + "'");
        }

        // Add the necessary columns
        DataColumnSpec classColumnSpec = inData[0].getDataTableSpec()
                .getColumnSpec(settings.getClassColumn());
        // The Rearranger adds new Columns, one for the Classes and the rest for
        // Probabilities
        ColumnRearranger c = createColumnRearranger(
                inData[1].getDataTableSpec(), classColumnSpec,
                inData[1].getRowCount());
        BufferedDataTable out = exec.createColumnRearrangeTable(inData[1], c,
                exec);

        return new BufferedDataTable[] { out };
    }

    /**
     * Transforms the training data into an internal representation.
     * 
     * @param data
     *            The training data
     * @throws Exception
     */
    private void createTrainingData(BufferedDataTable data) throws Exception
    {
        trainingData = new TrainingData[data.getRowCount()];

        int index = data.getDataTableSpec().findColumnIndex(
                settings.getClassColumn());
        int colCount = data.getDataTableSpec().getNumColumns() - 1;

        if (index == -1)
        {
            throw new Exception(
                    "The selected class column does not exist in the test data set.");
        }

        int i = 0;
        for (DataRow dataRow : data)
        {
            trainingData[i] = new TrainingData();
            trainingData[i].Class = ((StringValue) dataRow.getCell(index))
                    .getStringValue();
            trainingData[i].Data = new DataCell[colCount];

            int j = 0, k = 0;
            for (DataCell dataCell : dataRow)
            {
                if (j != index)
                {
                    trainingData[i].Data[k++] = dataCell;
                }

                j++;
            }

            i++;
        }
    }

    /**
     * Create the column mapping training data -> instance data
     * 
     * @param training
     * @param instance
     * @throws Exception
     */
    private void createColumnMapping(DataTableSpec training,
            DataTableSpec instance) throws InvalidSettingsException
    {
        columnMappig.clear();

        for (DataColumnSpec dataColumnSpec : training)
        {
            if (!dataColumnSpec.getName().equals(settings.getClassColumn()))
            {
                if (!instance.containsName(dataColumnSpec.getName()))
                {
                    throw new InvalidSettingsException("The column `"
                            + dataColumnSpec.getName()
                            + "` is missing in the instance data");
                }

                columnMappig.add(instance.findColumnIndex(dataColumnSpec
                        .getName()));
            }
        }
    }

    private DataCell[] GetClassValues(final DataColumnSpec classColumnSpec)
    {
        final DataCell[] possibleValues;
        // if Probabilities is turned on
        if (settings.isAddClassProbabilitiesColumns())
        {
            // get all the classes
            possibleValues = classColumnSpec.getDomain().getValues()
                    .toArray(new DataCell[0]);

            // Sort the classes
            Arrays.sort(possibleValues, new Comparator<DataCell>()
            {
                @Override
                public int compare(final DataCell o1, final DataCell o2)
                {
                    return o1.toString().compareTo(o2.toString());
                }
            });
        }
        else
        {
            possibleValues = new DataCell[0];
        }

        return possibleValues;
    }

    private List<DataColumnSpec> GetColumnsForClasses(final DataCell[] classes,
            final DataTableSpec instanceSpec)
    {
        final List<DataColumnSpec> colSpecs = new ArrayList<DataColumnSpec>();

        // Create the columns for each class
        for (DataCell cls : classes)
        {
            String newName = cls.toString();
            while (instanceSpec.containsName(newName))
            {
                newName += "_dup";
            }
            colSpecs.add(new DataColumnSpecCreator(newName, DoubleCell.TYPE)
                    .createSpec());
        }

        return colSpecs;
    }

    /**
     * Create a column rearranger which creates and fills the necessary
     * 
     * @param instanceSpec
     *            The instance data column specification
     * @param classColumnSpec
     *            The class column specification
     * @param maxRows
     * @return The column rearranger
     */
    private ColumnRearranger createColumnRearranger(DataTableSpec instanceSpec,
            final DataColumnSpec classColumnSpec, final double maxRows)
    {
        String className = settings.outputClassColumnModel.getStringValue();

        ColumnRearranger c = new ColumnRearranger(instanceSpec);
        List<DataColumnSpec> colSpecs = new ArrayList<DataColumnSpec>();

        // Create the class column
        DataColumnSpecCreator crea = new DataColumnSpecCreator(className,
                StringCell.TYPE);

        crea.setDomain(classColumnSpec.getDomain());
        colSpecs.add(crea.createSpec());
        
        if (settings.isAddProbabilityColumn())
        {
            colSpecs.add(new DataColumnSpecCreator("Probability",
                    DoubleCell.TYPE).createSpec());            
        }

        // Find the classes in the training data (if adding probability columns
        // is enabled)
        final DataCell[] possibleValues;
        if (settings.isAddClassProbabilitiesColumns())
        {
            possibleValues = GetClassValues(classColumnSpec);
            colSpecs.addAll(GetColumnsForClasses(possibleValues, instanceSpec));
        }
        else
        {
            possibleValues = new DataCell[0];
        }

        final DataColumnSpec[] colSpecArray = colSpecs
                .toArray(new DataColumnSpec[colSpecs.size()]);
        // fill the cells of the new Columns
        // Factory that creates the class cell
        CellFactory factory = new CellFactory()
        {
            @Override
            public void setProgress(final int curRowNr, final int rowCount,
                    final RowKey lastKey, final ExecutionMonitor exec)
            {
                exec.setProgress(curRowNr / maxRows, "Classifying row "
                        + lastKey);
            }

            @Override
            public DataCell[] getCells(DataRow row)
            {
                return FindClassForRow(row, possibleValues);
            }

            @Override
            public DataColumnSpec[] getColumnSpecs()
            {
                return colSpecArray;
            }
        };
        // add them
        c.append(factory);
        // and ready
        return c;
    }

    /**
     * Finds the class for the given instance using the k-NN algorithm with the
     * training data.
     * 
     * @param row
     *            The instance
     * @param possibleClasses
     *            The classes used in the training data
     * @return A value for each added columns
     */
    private DataCell[] FindClassForRow(DataRow row, DataCell[] possibleClasses)
    {
        DataCell[] completeRow = new DataCell[possibleClasses.length + 1 + (settings.isAddProbabilityColumn()? 1 : 0)];

        boolean weightByDistance = settings.isWeightingByDistance();
        int k = settings.getK();

        // Find the k nearest neighbors (ordered by their distance to the
        // instance)
        TrainingData[] neighbors = new TrainingData[k];
        for (TrainingData data : trainingData)
        {
            DataCell[] instanceValues = new DataCell[data.Data.length];

            int i = 0;
            for (Integer index : columnMappig)
            {
                instanceValues[i++] = row.getCell(index);
            }

            // Calculate the distance
            data.Distance = settings.getDistanceFunction().getDistance(
                    data.Data, instanceValues, settings);

            // Training node is closer than the farthest neighbor so far
            if (neighbors[0] == null || neighbors[0].Distance > data.Distance)
            {
                neighbors[0] = data;

                // Sort the new node into position (keep the order by distance)
                i = 0;
                while (++i < k
                        && (neighbors[i] == null || neighbors[i].Distance > neighbors[i - 1].Distance))
                {
                    TrainingData t = neighbors[i];
                    neighbors[i] = neighbors[i - 1];
                    neighbors[i - 1] = t;
                }
            }
        }

        // Table: class -> weight
        HashMap<String, MutableDouble> classes = new HashMap<String, MutableDouble>();
        double total = 0;

        for (TrainingData data : neighbors)
        {
            double weight = weightByDistance ? 1.0 / data.Distance
                    / data.Distance : 1;
            total += weight;

            if (classes.containsKey(data.Class))
            {
                classes.get(data.Class).add(weight);
            }
            else
            {
                classes.put(data.Class, new MutableDouble(weight));
            }
        }

        // Find the "heaviest" class
        String cls = null;
        double weight = 0;
        double totalWeight = 0;
        int index = 0;
        for (Entry<String, MutableDouble> clazz : classes.entrySet())
        {
            if (clazz.getValue().doubleValue() > weight)
            {
                weight = clazz.getValue().doubleValue();
                cls = clazz.getKey();
            }
            
            totalWeight += clazz.getValue().doubleValue();
        }
        // fill the first part of the row with the class
        completeRow[0] = new StringCell(cls);
        
        if (settings.isAddProbabilityColumn())
        {
            completeRow[1] = new DoubleCell(Double.isInfinite(weight)? 1 : (weight / totalWeight));
            index = 1;
        }

        // now add the probabilities
        while (index < possibleClasses.length)
        {
            // first: is it in the k radius?
            if (classes.containsKey(possibleClasses[index].toString()))
            {
                // add the probability
                completeRow[index + 1] = new DoubleCell(classes.get(
                        possibleClasses[index].toString()).doubleValue()
                        / total);
            }
            // if not, just fill in with 0
            else
            {
                completeRow[index + 1] = new DoubleCell(0);
            }
            index++;
        }
        // and ready
        return completeRow;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset()
    {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException
    {
        // Find the first string column in the training data and use it as class
        // column
        if (settings.getClassColumn() == null
                || !inSpecs[0].containsName(settings.getClassColumn()))
        {
            if (!inSpecs[0].containsCompatibleType(StringValue.class))
            {
                throw new InvalidSettingsException(
                        "No class column found in test data");
            }

            for (DataColumnSpec col : inSpecs[0])
            {
                if (col.getType().isCompatible(StringValue.class))
                {
                    settings.setClassColumn(col.getName());
                    break;
                }
            }
        }

        createColumnMapping(inSpecs[0], inSpecs[1]);

        // Ensure a valid class column name
        String className = settings.outputClassColumnModel.getStringValue();
        while (inSpecs[1].containsName(className))
        {
            className += "_dup";
        }

        // Add the necessary columns
        DataColumnSpec classColumnSpec = inSpecs[0].getColumnSpec(settings
                .getClassColumn());

        ColumnRearranger c = createColumnRearranger(inSpecs[1],
                classColumnSpec, 1);

        return new DataTableSpec[] { c.createSpec() };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings)
    {
        this.settings.saveSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
        this.settings.loadSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
        this.settings.validateSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException
    {
    }

}
