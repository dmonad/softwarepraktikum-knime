package de.rwth.dme.knn;

import org.knime.core.data.DataCell;

public interface IDistanceFunction
{
    public String getName();
    
    public double getDistance(DataCell[] a, DataCell[] b, KNNNodeSettings settings);

}
