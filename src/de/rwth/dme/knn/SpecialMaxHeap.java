/**
 * 
 */
package de.rwth.dme.knn;

/**
 * @author dtrn
 * This is a trivial implementation of MaxHeap.
 */
public class SpecialMaxHeap {
    private SpecialElement[] Heap;
    private int maxsize;
    private int size;

    public SpecialMaxHeap(int k) {
		maxsize = k;
		Heap = new SpecialElement[maxsize];
		size = 0 ;
    }
    
    private int leftchild(int pos) {
    	return 2*pos + 1;
    }
    private int rightchild(int pos) {
    	return 2*pos + 2;
    }

    private int parent(int pos) {
    	return  (pos-1) / 2;
    }
    
    private boolean isleaf(int pos) {
    	return ((pos >= size/2) && (pos < size));
    }

    private void swap(int pos1, int pos2) {
		SpecialElement tmp;
	
		tmp = Heap[pos1];
		Heap[pos1] = Heap[pos2];
		Heap[pos2] = tmp;
    }

    public SpecialElement[] getElements(){
    	return Heap;
    }
    
    public TrainingData[] getNeighbours(){
        TrainingData[] ps = new TrainingData[size];
    	for(int i=0; i<size; i++){
    		ps[i] = Heap[i].data;
    	}
    	return ps;
    }
    

    public void compareAndInsert(double distance, TrainingData data) {
        compareAndInsert(new SpecialElement(distance, data));
    }
    
    /*
     * Just add SpecialElements here. They will be compared and inserted, 
     * if they have less weight than the first one in the MaxHeap
     */
    public void compareAndInsert(SpecialElement elem) {
    	
    	if(size != 0 && elem.distance >= Heap[0].distance){
    		return;
    	}
    	
    	if(size == maxsize){
    		replacemin(elem);
    	}
		Heap[size++] = elem;
		int current = size;
		
		while (Heap[current].distance > Heap[parent(current)].distance) {
		    swap(current, parent(current));
		    current = parent(current);
		}	
	 }

    private void replacemin(SpecialElement elem) {
		Heap[0]=elem;
		if (size != 0)
		    pushdown(0);
    }

    private void pushdown(int position) {
		int biggestchild;
		while (!isleaf(position)) {
		    biggestchild = leftchild(position);
		    if ((biggestchild < size) && (Heap[biggestchild].distance < Heap[biggestchild+1].distance))
		    	biggestchild = biggestchild + 1;
		    if (Heap[position].distance >= Heap[biggestchild].distance) return;
		    swap(position,biggestchild);
		    position = biggestchild;
		}
	}

}


