/**
 * 
 */
package de.rwth.dme.knn;

/**
 * @author dtrn
 *
 */
public class SpecialElement {
	double distance;
	TrainingData data; // Change to row or something
	
	SpecialElement(double weight, TrainingData data){
		this.distance = weight;
		this.data = data;
	}
}

