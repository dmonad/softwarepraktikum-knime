package de.rwth.dme.knn;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Set;

import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.osgi.framework.BundleContext;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

import de.rwth.dme.knn.distance.*;

/**
 * This is the eclipse bundle activator.
 * Note: KNIME node developers probably won't have to do anything in here,
 * as this class is only needed by the eclipse platform/plugin mechanism.
 * If you want to move/rename this file, make sure to change the plugin.xml
 * file in the project root directory accordingly.
 * 
 * @author Manuel
 */
public class KNNNodePlugin extends Plugin
{
    // The shared instance.
    private static KNNNodePlugin plugin;

    /**
     * The constructor.
     */
    public KNNNodePlugin()
    {
        super();
        plugin = this;
    }

    private static final HashMap<String, IDistanceFunction> distanceFunctions = new HashMap<>();

    /**
     * Adds a new distance function
     * @param The distance function
     */
    public static void addDistanceFunction(IDistanceFunction function)
    {
        distanceFunctions.put(function.getName(), function);
    }

    /**
     * Removes a distance function
     * @param The distance function
     */
    public static void removeDistanceFunction(IDistanceFunction function)
    {
        distanceFunctions.remove(function.getName());
    }

    /**
     * Get a distance function by its name
     * @param The distance function's name
     */
    public static IDistanceFunction getDistanceFunction(String function)
    {
        return distanceFunctions.get(function);
    }

    /**
     * @return A list of names of all available distance functions
     */
    public static Set<String> getDistanceFunctions()
    {
        return distanceFunctions.keySet();
    }

    /**
     * This method is called upon plug-in activation.
     * 
     * @param context
     *            The OSGI bundle context
     * @throws Exception
     *             If this plugin could not be started
     */
    @Override
    public void start(final BundleContext context) throws Exception
    {
        super.start(context);

        // Load the distance functions from the preferences
        IEclipsePreferences preferences = ConfigurationScope.INSTANCE
                .getNode("de.rwth.dme.knn.KNNNodeFactory");

        if (!preferences.nodeExists("distanceFunctions"))
        {
            CreateDefaultPreferences(preferences);
        }

        Preferences pref = preferences.node("distanceFunctions");

        for (String name : pref.keys())
        {
            IDistanceFunction df = loadDistanceFunction(pref.get(name, null));

            addDistanceFunction(df);
        }
    }

    /**
     * Tries to load a distance function given the full class name and an optional
     * directory/jar. The format is "C:\some\dir\, some.package.SomeClass".
     *  
     * @param clsName The class path of the distance function
     * @return The distance function
     * 
     * @throws MalformedURLException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public IDistanceFunction loadDistanceFunction(String clsName)
            throws MalformedURLException, ClassNotFoundException,
            InstantiationException, IllegalAccessException
    {
        ClassLoader loader = this.getClass().getClassLoader();

        // Path contains a directory/jar to load the class from
        if (clsName.indexOf(',') != -1)
        {
            String[] t = clsName.split(",\\s*");
            clsName = t[1].trim();

            URL outputDirectory = new File(t[0].trim()).toURI().toURL();
            loader = new URLClassLoader(new URL[] { outputDirectory }, loader);
        }

        Class<?> cls = Class.forName(clsName, true, loader);

        return (IDistanceFunction) cls.newInstance();
    }

    /**
     * Creates the default list of distance functions 
     * @param preferences
     * @throws BackingStoreException
     */
    private void CreateDefaultPreferences(IEclipsePreferences preferences)
            throws BackingStoreException
    {
        Preferences pref = preferences.node("distanceFunctions");

        pref.put(new MinkowskiDistanceFunction().getName(),
                MinkowskiDistanceFunction.class.getName());
        pref.put(new CosineDistanceFunction().getName(),
                CosineDistanceFunction.class.getName());

        pref.flush();
    }

    /**
     * This method is called when the plug-in is stopped.
     * 
     * @param context
     *            The OSGI bundle context
     * @throws Exception
     *             If this plugin could not be stopped
     */
    @Override
    public void stop(final BundleContext context) throws Exception
    {
        super.stop(context);
        plugin = null;
    }

    /**
     * Returns the shared instance.
     * 
     * @return Singleton instance of the Plugin
     */
    public static KNNNodePlugin getDefault()
    {
        return plugin;
    }

}
