package de.rwth.dme.knn.distance;

import org.knime.core.data.DataCell;
import org.knime.core.data.def.DoubleCell;

import de.rwth.dme.knn.IDistanceFunction;
import de.rwth.dme.knn.KNNNodeSettings;

public class CosineDistanceFunction implements IDistanceFunction
{
    @Override
    public String getName()
    {
        return "Cosine";
    }

    @Override
    public double getDistance(DataCell[] as, DataCell[] bs, KNNNodeSettings settings)
    {
        double AB = 0, A = 0, B = 0;
        
        for (int i = 0; i < as.length; i++)
        {
            double a = ((DoubleCell)as[i]).getDoubleValue();
            double b = ((DoubleCell)bs[i]).getDoubleValue();
            
            AB += a * b;
            A += a * a;
            B += b * b;
        }

        return 1 - (AB / Math.sqrt(A) / Math.sqrt(B));
    }

}
