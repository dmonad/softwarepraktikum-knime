package de.rwth.dme.knn.distance;

import org.knime.core.data.DataCell;
import org.knime.core.data.def.DoubleCell;

import de.rwth.dme.knn.IDistanceFunction;
import de.rwth.dme.knn.KNNNodeSettings;


public class MinkowskiDistanceFunction implements IDistanceFunction
{
    public String getName()
    {
        return "Minkowski";
    }
    
    @Override
    public double getDistance(DataCell[] a, DataCell[] b, KNNNodeSettings settings)
    {
        double sum = 0;
        int p = settings.pForMinkowski.getIntValue();
        
        for (int i = 0; i < a.length; i++)
        {
            double diff = ((DoubleCell)a[i]).getDoubleValue() - ((DoubleCell)b[i]).getDoubleValue();
            
            sum += Math.pow(diff, p);            
        }

        return Math.pow(sum, 1.0/p);
    }
    
}
