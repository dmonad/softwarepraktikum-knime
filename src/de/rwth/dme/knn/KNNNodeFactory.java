package de.rwth.dme.knn;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "KNN" Node.
 * 
 *
 * @author  Daniel, Manuel
 */
public class KNNNodeFactory 
        extends NodeFactory<KNNNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public KNNNodeModel createNodeModel() {
        return new KNNNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<KNNNodeModel> createNodeView(final int viewIndex,
            final KNNNodeModel nodeModel) {
        return new KNNNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new KNNNodeDialog();
    }

}

